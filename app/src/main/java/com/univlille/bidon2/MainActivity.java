package com.univlille.bidon2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.univlille.bidon2.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // la classe ActivityMainBinding est créée automatiquement dès qu'on active le view binding dans
    // le grade et que le projet a été 'buildé'
    // le nom de la classe = NomDuLayout(sans _)Binding
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // devenu inutile grâce au view binding
        //      setContentView(R.layout.activity_main);
        // remplacé par la ligne ci-desous ainsi que setContentView(view); plus bas
        binding = ActivityMainBinding.inflate(getLayoutInflater());

        // grâce au view binding, tous les obktes avec unid sont directement accessibles dorénavant
        // sans passer par un findViewById().
        // getRoot renvoie le premier noeud du layout (ConstraintLayout par exemple)
        View view = binding.getRoot();
        // remplace le setContentView(R.layout.activity_main) d'avant
        setContentView(view);
        // on accède dorénavant directement aux objets avec id :
        binding.monTexte.setText("coucou");
        binding.monBouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "clic sur bouton", Toast.LENGTH_SHORT).show();
                binding.monTexte.setText("clic !!!!");
            }
        });
    }
}